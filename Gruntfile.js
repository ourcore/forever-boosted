module.exports = function(grunt) {

    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Convert Sass to CSS
        sass: {
            build: {
                options: {
                    // sourcemap: 'auto'
                },
                files: {
                    'src/css/styles.css': 'src/scss/main.scss'
                }
            }
        },

        // Add vendor prefixes to properties
        postcss: {
            options: {
                map: false,
                processors: [
                    require('autoprefixer')({
                        browsers: ['> 1%', 'last 2 versions', 'iOS 8']
                    })
                ]
            },
            dist: {
                src: 'src/css/*.css'
            }
        },

        // Minify CSS
        cssmin: {
            options: {
              sourceMap: false
            },
            target: {
                files: [{
                    expand: true,
                    cwd: 'src/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'public/css',
                    ext: '.min.css'
                }]
            }
        },

        // Minify JavaScript
        uglify: {
            my_target: {
                files: {
                    'public/js/scripts.min.js': ['src/js/jquery.smoothState.js', 'src/js/dialog-polyfill.js', 'src/js/focus-visible.js', 'src/js/scripts.js']
                }
            }
        },

        // Compress images
        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 3,
                    svgoPlugins: [{ removeViewBox: false }]
                },
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    src: ['*.*'],
                    dest: 'public/img/'
                }]
            }
        },

        // Launch Browsersync through NGINX
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'public/css/*.css',
                        'public/js/*.js',
                        'public/img/*',
                        'public/*.html',
                        'public/*/*.html'
                    ]
                },
                options: {
                    watchTask: true,
                    // port: '4000',
                    // proxy: "localhost:7888"
                    server: "public"
                }
            }
        },

        // Watch files for processing
        watch: {
            gruntfile: {
                files: ['Gruntfile.js'],
                options: {
                    reload: true
                }
            },
            css: {
                files: ['src/scss/*.scss', 'src/scss/*/*.scss', 'src/css/*.css'],
                tasks: ['css']
            },
            js: {
                files: ['src/js/*.js'],
                tasks: ['js']
            },
            img: {
                files: ['src/img/*'],
                tasks: ['newer:imagemin']
            }
        }
    });

    // Register tasks and related tasks, if any
    grunt.registerTask('default', ['browserSync', 'watch', 'imagemin']);
    grunt.registerTask('css', ['sass', 'postcss', 'cssmin']);
    grunt.registerTask('js', ['uglify']);
};
