var navHeight = $('nav#f-b-nav').outerHeight();

// smoothState.js
$(function(){
  'use strict';
  var options = {
    prefetch: true,
    cacheLength: 2,
    onStart: {
      duration: 250, // Duration of our animation
      render: function ($container) {
        // Add your CSS animation reversing class
        $container.addClass('is-exiting');

        // Restart your animation
        smoothState.restartCSSAnimations();
      }
    },
    onReady: {
      duration: 0,
      render: function ($container, $newContent) {
        // Remove your CSS animation reversing class
        $container.removeClass('is-exiting');

        // Inject the new content
        $container.html($newContent);

      }
    }
  },
  smoothState = $('#main').smoothState(options).data('smoothState');
});

// Page scroll percentage loading bar
// function navScrollBar() {
//   document.onscroll = function(){ 
//     var pos = getVerticalScrollPercentage(document.body);
//     $('#nav-loading-bar').css('width', Math.round(pos) + '%');
//   };

//   function getVerticalScrollPercentage(elm){
//     var p = elm.parentNode,
//         pos = (elm.scrollTop || p.scrollTop) / (p.scrollHeight - p.clientHeight ) * 100
    
//     return pos
//   }
// }

// Page load percentage bar
function pageLoadBar() {
  var width = 100,
  perfData = window.performance.timing, // The PerformanceTiming interface represents timing-related performance information for the given page
  EstimatedTime = -(perfData.loadEventEnd - perfData.navigationStart),
  time = parseInt((EstimatedTime/1000)%60)*100;

  // Load bar animation
  $('#page-load-bar').animate({
    width: width + '%'
  }, time);

  // Fade out load bar when finised
  setTimeout(function(){
    $('#page-load-bar').fadeOut(300);
  }, time);
}

// Nav placeholder for fixed
function navPlaceholder() {
  // var navHeight = $('nav#f-b-nav').outerHeight();
  $('#nav-placeholder').outerHeight(navHeight);
}

// Nav active links
function navActiveLink() {
  $('#nav-links > .nav-link-item > a').each(function() {
    if(this.pathname == window.location.pathname) {
      $(this).addClass('active');
    }
  });
}

// Toggle mobile nav menu
function mobileNavToggle() {
  $('#nav-btn').on('click', function() {
    // Toggle dropdown menu
    $('#nav-links').slideToggle();
  
    // Toggle nav button icon
    $(this).toggleClass('is-active');
  
    // Toggle ARIA label
    if($(this).hasClass('is-active')) {
        $(this).attr('aria-expanded', 'true');
    } else {
        $(this).attr('aria-expanded', 'false');
    }
  });
}

// Close mobile nav menu on page scroll
function mobileNavScrollClose() {
  $(window).scroll(function() {
    if($('#nav-btn').is(':visible') === true && $('#nav-links').is(':visible') === true) {
      // Toggle mobile nav menu
      $('#nav-links').slideUp();
    
      // Toggle nav button icon
      $('#nav-btn').removeClass('is-active');
    
      // Toggle ARIA label
      $('#nav-btn').attr('aria-expanded', 'false');
    }
  });
}

// Anchor (on click) scroll
function anchorClickScroll() {
  $('a[href*="#"]').on('click', function(event) {
    var hash = this.hash;
        padding = parseInt($('main.two-col').css('padding-top'));
    
    if(this.hash) {
      event.preventDefault();

      // Loop through questions, check if clicked hash matches a question ID, and expand it if so
      faqItemLoop(this.hash.replace('#', ''));

      // Scroll page to anchor
      $('html, body').animate({
        // Scroll to top of clicked anchor minus nav height and `main` padding
        scrollTop: $(hash).offset().top - (navHeight + padding)
      }, 350);

      // Update the URL in address bar
      window.location.hash = this.hash;
    }
  });
}

// Anchor (in URL) scroll
function anchorURLScroll() {
  if(location.hash.length) {
    var anchorID = location.hash.replace('#', ''),
        // navHeight = $('nav#f-b-nav').outerHeight(),
        padding = parseInt($('.two-col').css('padding-top'));

    // Scroll down to anchor
    $('html, body').animate({scrollTop: $('#' + anchorID).offset().top - (navHeight + padding)
    }, 500);
  }
}

// Section in view active link
function sectionIsInView() {
  var smallColLink = document.querySelectorAll('.small-col > .small-col-placeholder > ul > li > a'),
      twoColPadding = parseInt($('main.two-col').css('padding-top'));

  // Set active/in-view `.two-col` nav link on desktop/tablets (landscape) based on section distance from top of page (minus nav and padding)
  var fromTop = window.scrollY + navHeight + twoColPadding;

  smallColLink.forEach(function(link) {
    var section = document.querySelector(link.hash);

    if(section.offsetTop <= fromTop && section.offsetTop + section.offsetHeight > fromTop) {
      link.classList.add('active-section');
    } else{
      link.classList.remove('active-section');
    }
  });
}

// FAQ question loop *** UTILITY FUNCTION ***
function faqItemLoop(anchorID) {
  $('.faq-item').each(function() {
    if($(this).attr('id') === anchorID) {
      // Toggle question container classes and show answer
      $(this).children('.faq-q').toggleClass('faq-q-collapsed faq-q-expanded').siblings('.faq-a').slideToggle();

      // Toggle chevron icon
      $(this).children('.faq-q').children('i.faq-q-chevron').toggleClass('fa-chevron-down fa-chevron-up');
    }
  });
}

// FAQ question toggle (on click)
function faqClickToggle() {
  if($('.faq-item').length) {
    $('.faq-q-btn').on('click', function() {
      // Toggle question container collapsed/expanded classes
      $(this).parents('.faq-q').toggleClass('faq-q-collapsed faq-q-expanded');

      // Slide answer
      faqA = $(this).parents('.faq-q').siblings('.faq-a').slideToggle();

      // Toggle chevron icon
      $(this).parents('.faq-q').children('i.faq-q-chevron').toggleClass('faq-q-chevron-up')
      // .toggleClass('fa-chevron-down fa-chevron-up');
    });
  }
}

// FAQ question toggle (anchor in URL)
function faqAnchorToggle() {
  if($('body#faq-page').length && location.hash.length) {
    // Loop through questions, check if hash in URL matches a question ID, and expand it if it does
    faqItemLoop(location.hash.replace('#', ''));
  }
}

// FAQ question link clipboard
function copyToClipboard() {
  $('button.faq-q-clipboard').on('click', function() {
    // Create temporary input field
    var $tempElement = $('<input>'),
        currentURL = location.href.replace(location.hash, '') + '#';

    // Render temp input field
    $('body').append($tempElement);

    // Set the temp input field's text as the question ID and select it
    // $tempElement.val('foreverboosted.net/faq.html#' + $(this).parents('.faq-item').attr('id')).select();
    $tempElement.val(currentURL + $(this).parents('.faq-item').attr('id')).select();

    // Copy the question ID to the clipboard
    document.execCommand("Copy");

    // Remove the temp input field
    $tempElement.remove();

    // Change tooltip text
    $(this).attr('data-tooltip', 'Link question copied!').addClass('last-clicked');

    if($('#nav-btn').is(':visible') === true) {
      setTimeout(function() {
        //
      }, 1500);
    }

    // // Re-register dialog element due to smoothState.js
    // var dialog = document.querySelector('dialog');
    // dialogPolyfill.registerDialog(dialog);

    // // Show pop-up
    // // dialog.showModal();
    // dialog.show();

    // // Close pop-up
    // setTimeout(function() {
    //   dialog.close();
    // }, 1500);
  });
}

// YouTube video image -> embed
function ytVideo() {
  $('.youtube-video').on('click', function() {
    var videoID = $(this).find('.youtube-vid-tn').attr('src').split('/')[4] + '?autoplay=1';

    $(this).replaceWith('<div class="responsive-cont youtube-video"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + videoID + '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen playsinline></iframe></div>');
  });
}

// Sitewide dark mode
function darkMode() {
  // Check for saved darkMode value in localStorage
  var darkMode = localStorage.getItem('darkMode'),
      darkModeToggle = document.querySelector('#dark-mode-toggle');

  var enableDarkMode = function() {
    // 1. Add the class to the body
    document.body.classList.add('dark-mode');
    // document.querySelector('#main').classList.add('dark-mode')

    // 2. Update darkMode in localStorage
    localStorage.setItem('darkMode', 'enabled');

    // 3. Update dark mode labels
    document.querySelector('#dark-mode-label').textContent='Enable Light Mode';
    document.querySelector('#dark-mode-btn').setAttribute('aria-label', 'Enable light mode');
    document.querySelector('#dark-mode-btn').setAttribute('data-tooltip', 'Enable light mode');
  }

  var disableDarkMode = function() {
    // 1. Remove the class from the body
    document.body.classList.remove('dark-mode');
    // document.querySelector('#main').classList.remove('dark-mode')

    // 2. Update darkMode in localStorage 
    localStorage.setItem('darkMode', null);

    // 3. Update dark mode labels
    document.querySelector('#dark-mode-label').textContent='Enable Dark Mode';
    document.querySelector('#dark-mode-btn').setAttribute('aria-label', 'Enable dark mode');
    document.querySelector('#dark-mode-btn').setAttribute('data-tooltip', 'Enable dark mode');
  }

  // If the user already visited and enabled darkMode, start things off with it enabled
  if(darkMode === 'enabled') {
    enableDarkMode();
  }

  // When someone clicks the button
  darkModeToggle.addEventListener('click', function() {
    // Get the darkMode setting
    darkMode = localStorage.getItem('darkMode');

    // If dark mode's not currently enabled, enable it
    if(darkMode !== 'enabled') {
      enableDarkMode();
    // if it hasn't been enabled, turn it off  
    } else {  
      disableDarkMode(); 
    }

    // Remove button focus
    document.querySelector('#dark-mode-btn').blur();
  });
}

// Scroll to top button (scroll listener)
var lastScrollTop = 0;
function scrollToTopListener() {
  // Check if the window is at the top (scrolled more than nav height); if not, display button
  // https://stackoverflow.com/questions/61721191/anchor-scroll-function-interferes-with-scroll-event-listener-logic-to-hide-show
  var scrollTop = $(document).scrollTop(),
      scrollUp = scrollTop < lastScrollTop;

  lastScrollTop = scrollTop;

  if(scrollTop > navHeight && !scrollUp) {
    $('#scroll-top-btn').fadeIn();
  }else if(scrollTop < navHeight && scrollUp) {
    $('#scroll-top-btn').fadeOut();
  }
}

// Scroll to top button (on click)
function scrollToTopClick() {
  // Click event to scroll to top
  $('#scroll-top-btn').click(function(){
    $('html, body').animate({
      scrollTop: 0
    },350);

    return false;
  });
}

// Run on page load
$(document).ready(function() {
  pageLoadBar();
  navPlaceholder();
  navActiveLink();
  mobileNavToggle();
  mobileNavScrollClose();
  anchorClickScroll();
  anchorURLScroll();
  sectionIsInView();
  faqClickToggle();
  copyToClipboard();
  faqAnchorToggle();
  ytVideo();
  darkMode();
  scrollToTopClick();
});

/* When you run a plugin on $(document).ready(), it's going to register only on elements that are currently on the page. Since we're injecting new elements every load, we need to run the plugins again, scoping it to just the new stuff.
A good way to do this is to wrap your plugin initializations in a function that we call on both $.fn.ready() and onAfter. You'll want to specify the context each time you initialize the plugins so that you don't double-bind them. This is called a "module execution controller".
*** Using context, which sets function's scope, isn't as important since I'm using IDs for most/all selectors *** */
$('#main').smoothState({
  onAfter: function() {
    pageLoadBar();
    navPlaceholder();
    navActiveLink();
    mobileNavToggle();
    mobileNavScrollClose();
    anchorClickScroll();
    anchorURLScroll();
    sectionIsInView();
    faqClickToggle();
    copyToClipboard();
    faqAnchorToggle();
    ytVideo();
    darkMode();
    scrollToTopClick();
  }
});

// Run on page scroll
$(window).scroll(function() {
  sectionIsInView();
  scrollToTopListener();
});